# Data are milli Pa s not c Pa s
LiF Viscosity experimental data

![](https://gitlab.com/apw951/lif-viscosity-data/-/raw/main/experimental.png?ref_type=heads)

Sources

- Tatsuhiko Ejima, Yuzuru Sato, Seiji Yaegashi, Takashi Kijima, Eiji Takeuchi, Kyoko Tamai, Viscosity of Molten Alkali Fluorides, Journal of the Japan Institute of Metals and Materials, 1987, Volume 51, Issue 4, Pages 328-337, Released on J-STAGE April 04, 2008, Online ISSN 1880-6880, Print ISSN 0021-4876, https://doi.org/10.2320/jinstmet1952.51.4_328, https://www.jstage.jst.go.jp/article/jinstmet1952/51/4/51_4_328/_article/-char/en

- Ana-Maria Popescu & Virgil Constantin (2015) Viscosity of Alkali Fluoride Ionic Melts at Temperatures up to 373.15 K above Melting Points, Chemical Engineering Communications, 202:12, 1703-1710, DOI: 10.1080/00986445.2014.970254

- Yoshiyuki Abe, Otoya Kosugiyama, Akira Nagashima, Viscosity of LiF-BeF2 eutectic mixture(XBeF2 = 0.328) and LiF single salt at elevated temperatures, Journal of Nuclear Materials, Volume 99, Issues 2–3, 1981, Pages 173-183, ISSN 0022-3115, https://doi.org/10.1016/0022-3115(81)90186-0. (https://www.sciencedirect.com/science/article/pii/0022311581901860)

- Janz, G. J.; Allen, C. B.; Downey, J. R., Jr.; Tomkins, R. P. T. Physical Properties Data Compilations Relevant to Energy Storage; NSRDS, 1978

Each data files consists of temperature (first column), in Kelvin, and viscosity (second column) data, in  cPa s
